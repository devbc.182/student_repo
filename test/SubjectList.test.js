//Import the SubjectList smart contract
const SubjectList = artifacts.require('SubjectList')

contract('SubjectList', (accounts) => {
    // make sure contract is deployed and before we retrive the subjectlist object for testing
    beforeEach(async () => {
        this.subjectList = await SubjectList.deployed()
    })

    //Testing the deployed subject contract
    it('deploys successfully', async () => {
        //Get the address which the subject object is stored
        const address = await this.subjectList.address
        //Test for valid address
        isValidAddress(address)
    })

    //Testing the content in the contract
    // it('test adding subject', async () => {
    //     return SubjectList.deployed().then ((instance) => {
    //         s = instance;
    //         code = "CSB203";
    //         return s.createSubject(code, "DApp");
    //     }).then((transaction) => {
    //         isValidAddress(transaction.tx)
    //         isValidAddress(transaction.receipt.blockHash);
    //         return s.subjectsCount()
    //     }).then((count) => {
    //         assert.equal(count, 1)
    //         return s.subjects(1);
    //     }).then((subject) => {
    //         assert.equal(subject.code, code)
    //     })
    // })
    it('test adding subject', async () => {
        const s = await SubjectList.deployed();
        const code = 'CSB203';
        const name = 'DApp';
      
        const { tx, receipt } = await s.createSubject(code, name);
      
        assert.exists(tx);   //Should exist hashed transaction
        assert.exists(receipt.blockHash);    //Block hash should exist
      
        const count = await s.subjectsCount();
        assert.equal(count, 1); // Subjects count should be 1
      
        const subject = await s.subjects(1);
        assert.equal(subject.code, code);    //Subject code should match the input
        assert.equal(subject.name, name);    //Subject name should match the input
        assert.equal(subject.retired, false);  //Subject should not be retired
    })

    //Testing changing content in the contract
    it('test mark retired subjects', async () => {
        return SubjectList.deployed().then(async (instance) =>{
            s = instance;
            return s.findSubject(1).then(async (osubject) => {
                assert.equal(osubject.name, "DApp")
                assert.equal(osubject.retired, false)
                return s.markRetired(1).then(async (transaction) => {
                    return s.findSubject(1).then(async (nsubject) => {
                        assert.equal(nsubject.name, "DApp")
                        assert.equal(nsubject.retired, true)
                    })
                })
            })
        })
    })
    //Testing the findSubject function
    it('test finding subjects', async () => {
        return SubjectList.deployed().then (async (instance) => {   //
            s.instance;
            code = "CSB203";
            return s.createSubject("CSB202", "Intro. to BC").then(async (tx) => {
                return s.createSubject("ELE101", "Cyber").then(async (tx) => {
                    return s.createSubject("ACT201", "Analytical and Critical Thinking").then(async (tx) => {
                        return s.subjectsCount().then(async (count) => {
                            assert.equal(count, 4)
                            return s.findSubject(4).then(async (subject) => {
                                assert.equal(subject.code, "ACT201")
                            })
                        })
                    })
                })
            })
        })
    })

    //Testint the updateSubjectCode function
    it('test update subjects code', async () => {
        return SubjectList.deployed().then(async (instance) => {
            s = instance;
            return s.findSubject(1).then(async (osubject) => {
                assert.equal(osubject.name, "DApp")
                assert.equal(osubject.code, "CSB203")
                return s.updateSubjectCode(1, "CSB204", "DApp2").then(async (transaction) => {
                    return s.findSubject(1).then(async (nsubject) => {
                        assert.equal(nsubject.name, "DApp2")
                        assert.equal(nsubject.code, "CSB204")
                    })
                })
            })
        })
    })
})

// This function check if the address is valid
function isValidAddress(address) {
    assert.notEqual(address, 0x0)
    assert.notEqual(address, '')
    assert.notEqual(address, null)
    assert.notEqual(address, undefined)
}
